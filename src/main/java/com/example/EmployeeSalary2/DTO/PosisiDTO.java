package com.example.EmployeeSalary2.DTO;

public class PosisiDTO {
	
	private int idPosisi;
	private String namaPosisi;
	
	public PosisiDTO() {
		super();
	}
	
	public PosisiDTO(int idPosisi, String namaPosisi) {
		super();
		this.idPosisi = idPosisi;
		this.namaPosisi = namaPosisi;
	}

	public int getIdPosisi() {
		return idPosisi;
	}

	public void setIdPosisi(int idPosisi) {
		this.idPosisi = idPosisi;
	}

	public String getNamaPosisi() {
		return namaPosisi;
	}

	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
	
	
	

}
