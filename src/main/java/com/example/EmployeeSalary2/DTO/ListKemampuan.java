package com.example.EmployeeSalary2.DTO;


public class ListKemampuan {

	private int idListKemampuan;
	private KaryawanDTO karyawan;
	private KemampuanDTO kemampuan;
	private Integer nilaiKemampuan;
	
	
	public ListKemampuan() {
		super();
	}


	public ListKemampuan(int idListKemampuan, KaryawanDTO karyawan, KemampuanDTO kemampuan, Integer nilaiKemampuan) {
		super();
		this.idListKemampuan = idListKemampuan;
		this.karyawan = karyawan;
		this.kemampuan = kemampuan;
		this.nilaiKemampuan = nilaiKemampuan;
	}


	public int getIdListKemampuan() {
		return idListKemampuan;
	}


	public void setIdListKemampuan(int idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}


	public KaryawanDTO getKaryawan() {
		return karyawan;
	}


	public void setKaryawan(KaryawanDTO karyawan) {
		this.karyawan = karyawan;
	}


	public KemampuanDTO getKemampuan() {
		return kemampuan;
	}


	public void setKemampuan(KemampuanDTO kemampuan) {
		this.kemampuan = kemampuan;
	}


	public Integer getNilaiKemampuan() {
		return nilaiKemampuan;
	}


	public void setNilaiKemampuan(Integer nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}
	
	
	
	
	
	
}
