package com.example.EmployeeSalary2.DTO;

import java.util.Date;

import com.example.EmployeeSalary2.models.Agama;
import com.example.EmployeeSalary2.models.Penempatan;
import com.example.EmployeeSalary2.models.Posisi;
import com.example.EmployeeSalary2.models.Tingkatan;

public class KaryawanDTO {
	
	private int idKaryawan;
	private AgamaDTO agama;
	private PenempatanDTO penempatan;
	private PosisiDTO posisi;
	private TingkatanDTO tingkatan;
	private String nama;
	private String noKtp;
	private String alamat;
	private Date tanggalLahir;
	private Integer masaKerja;
	private Short statusPernikahan;
	private Date kontrakAwal;
	private Date kontrakAkhir;
	private String jenisKelamin;
	private Integer jumlahAnak;
	
	public KaryawanDTO() {
		super();
	}

	public KaryawanDTO(int idKaryawan, AgamaDTO agama, PenempatanDTO penempatan, PosisiDTO posisi, TingkatanDTO tingkatan,
			String nama, String noKtp, String alamat, Date tanggalLahir, Integer masaKerja, Short statusPernikahan,
			Date kontrakAwal, Date kontrakAkhir, String jenisKelamin, Integer jumlahAnak) {
		super();
		this.idKaryawan = idKaryawan;
		this.agama = agama;
		this.penempatan = penempatan;
		this.posisi = posisi;
		this.tingkatan = tingkatan;
		this.nama = nama;
		this.noKtp = noKtp;
		this.alamat = alamat;
		this.tanggalLahir = tanggalLahir;
		this.masaKerja = masaKerja;
		this.statusPernikahan = statusPernikahan;
		this.kontrakAwal = kontrakAwal;
		this.kontrakAkhir = kontrakAkhir;
		this.jenisKelamin = jenisKelamin;
		this.jumlahAnak = jumlahAnak;
	}

	public int getIdKaryawan() {
		return idKaryawan;
	}

	public void setIdKaryawan(int idKaryawan) {
		this.idKaryawan = idKaryawan;
	}

	public AgamaDTO getAgama() {
		return agama;
	}

	public void setAgama(AgamaDTO agama) {
		this.agama = agama;
	}

	public PenempatanDTO getPenempatan() {
		return penempatan;
	}

	public void setPenempatan(PenempatanDTO penempatan) {
		this.penempatan = penempatan;
	}

	public PosisiDTO getPosisi() {
		return posisi;
	}

	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}

	public TingkatanDTO getTingkatan() {
		return tingkatan;
	}

	public void setTingkatan(TingkatanDTO tingkatan) {
		this.tingkatan = tingkatan;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Date getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public Integer getMasaKerja() {
		return masaKerja;
	}

	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}

	public Short getStatusPernikahan() {
		return statusPernikahan;
	}

	public void setStatusPernikahan(Short statusPernikahan) {
		this.statusPernikahan = statusPernikahan;
	}

	public Date getKontrakAwal() {
		return kontrakAwal;
	}

	public void setKontrakAwal(Date kontrakAwal) {
		this.kontrakAwal = kontrakAwal;
	}

	public Date getKontrakAkhir() {
		return kontrakAkhir;
	}

	public void setKontrakAkhir(Date kontrakAkhir) {
		this.kontrakAkhir = kontrakAkhir;
	}

	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public Integer getJumlahAnak() {
		return jumlahAnak;
	}

	public void setJumlahAnak(Integer jumlahAnak) {
		this.jumlahAnak = jumlahAnak;
	}

	


}
