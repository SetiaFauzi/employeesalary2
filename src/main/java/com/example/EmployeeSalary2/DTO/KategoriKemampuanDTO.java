package com.example.EmployeeSalary2.DTO;

public class KategoriKemampuanDTO {
	
	private int idKategori;
	private String namaKategori;
	
	
	public KategoriKemampuanDTO() {
		super();
	}


	public KategoriKemampuanDTO(int idKategori, String namaKategori) {
		super();
		this.idKategori = idKategori;
		this.namaKategori = namaKategori;
	}


	public int getIdKategori() {
		return idKategori;
	}


	public void setIdKategori(int idKategori) {
		this.idKategori = idKategori;
	}


	public String getNamaKategori() {
		return namaKategori;
	}


	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
	
	
	
	

}
