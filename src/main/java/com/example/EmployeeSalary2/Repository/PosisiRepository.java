package com.example.EmployeeSalary2.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary2.models.Posisi;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi, Integer> {

}
