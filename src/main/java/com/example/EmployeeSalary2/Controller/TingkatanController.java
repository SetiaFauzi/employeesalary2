package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary2.DTO.PosisiDTO;
import com.example.EmployeeSalary2.DTO.TingkatanDTO;
import com.example.EmployeeSalary2.Repository.TingkatanRepository;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary2.models.Posisi;
import com.example.EmployeeSalary2.models.Tingkatan;

@RestController
@RequestMapping("/api")
public class TingkatanController {
	
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	
	
	 @GetMapping("/tingkatan/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			
			ArrayList<Tingkatan> listTingkatanEntity = (ArrayList<Tingkatan>) tingkatanRepository.findAll();
			ArrayList<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
			
			for(Tingkatan tingkatan : listTingkatanEntity) {
				TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
				listTingkatanDTO.add(tingkatanDTO);
			}
			
			result.put("Status", 200);
			result.put("Message", "Read all Posisi data succes");
			result.put("Data", listTingkatanEntity);
			
			return result;
			
	 }
	 
	 @PostMapping("/tingkatan/create")
		public HashMap<String, Object> createTingkatanDTOMaping(@Valid @RequestBody TingkatanDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Tingkatan tingkatanEntity = modelMapper.map(body, Tingkatan.class);
			tingkatanRepository.save(tingkatanEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New Tingkatan Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	 
	 @PutMapping("/tingkatan/update/{id}")
		public HashMap<String, Object> updateTingkatanDTOMaping(@PathVariable(value = "id") Integer idTingkatan,
	            @Valid @RequestBody TingkatanDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan)
					.orElseThrow(() -> new ResourceNotFoundException ("Tingkatan", "id", idTingkatan));
			
			tingkatanEntity = modelMapper.map(body, Tingkatan.class);
			tingkatanEntity.setIdTingkatan(idTingkatan);
			
			tingkatanRepository.save(tingkatanEntity);
			body = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New tingkatan Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	
	 @DeleteMapping("/tingkatan/delete/{id}")
		public HashMap<String, Object> deleteTingkatanDTOMaping(@PathVariable(value = "id") Integer idTingkatan,
				TingkatanDTO tingkatanDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan)
					.orElseThrow(() -> new ResourceNotFoundException ("Tingkatan", "id", idTingkatan));
			
			tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
			tingkatanEntity.setIdTingkatan(idTingkatan);
			
			tingkatanRepository.delete(tingkatanEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of Posisi is Succesed");
			
			return result;
		
	 }
	

	
	

}
