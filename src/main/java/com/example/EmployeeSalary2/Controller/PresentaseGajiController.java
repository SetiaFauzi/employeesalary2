package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary2.DTO.AgamaDTO;
import com.example.EmployeeSalary2.DTO.PresentaseGajiDTO;
import com.example.EmployeeSalary2.Repository.PresentaseGajiRepository;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary2.models.Agama;
import com.example.EmployeeSalary2.models.PresentaseGaji;


@RestController
@RequestMapping("/api")
public class PresentaseGajiController {
	
 ModelMapper modelMapper = new ModelMapper();
	 
	 @Autowired
	 PresentaseGajiRepository presGajiRepos;
	 
	 
	 @GetMapping("/presentaseGaji/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			ModelMapper modelMapper = new ModelMapper();
			
			ArrayList<PresentaseGaji> listPresGajiEntity = (ArrayList<PresentaseGaji>) presGajiRepos.findAll();
			ArrayList<PresentaseGajiDTO> listPresGajiDTO = new ArrayList<PresentaseGajiDTO>();
			
			for(PresentaseGaji presentaseGaji : listPresGajiEntity) {
				PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
				listPresGajiDTO.add(presentaseGajiDTO);
			}
			
			result.put("Status", 200);
			result.put("Message", "Read all Presentase Gaji data succes");
			result.put("Data", listPresGajiDTO);
			
			return result;
			
	 }
	 
	 @PostMapping("/presentaseGaji/create")
		public HashMap<String, Object> createPresGajiDTOMaping(@Valid @RequestBody PresentaseGajiDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			PresentaseGaji presGajiEntity = modelMapper.map(body, PresentaseGaji.class);
			presGajiRepos.save(presGajiEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New Presentase Gaji Succes");
			result.put("Data", body);
			
			return result;
			
			
	 }
	 
	 @PutMapping("/presentaseGaji/update/{id}")
		public HashMap<String, Object> updatePresGajiDTOMaping(@PathVariable(value = "id") Integer idPresentaseGaji,
	            @Valid @RequestBody PresentaseGajiDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			PresentaseGaji presentaseGajiEntity = presGajiRepos.findById(idPresentaseGaji)
					.orElseThrow(() -> new ResourceNotFoundException ("PresentaseGaji", "id", idPresentaseGaji));
			
			presentaseGajiEntity = modelMapper.map(body, PresentaseGaji.class);
			presentaseGajiEntity.setIdPresentaseGaji(idPresentaseGaji);
			
			presGajiRepos.save(presentaseGajiEntity);
			body = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New presentaseGaji Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	 
	 @DeleteMapping("/presentaseGaji/delete/{id}")
		public HashMap<String, Object> deletPresGajiDTOMaping(@PathVariable(value = "id") Integer idPresentaseGaji,
				PresentaseGajiDTO presentaseGajiDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			PresentaseGaji presentaseGajiEntity = presGajiRepos.findById(idPresentaseGaji)
					.orElseThrow(() -> new ResourceNotFoundException ("PresentaseGaji", "id", idPresentaseGaji));
			
			presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
			presentaseGajiEntity.setIdPresentaseGaji(idPresentaseGaji);
			
			presGajiRepos.delete(presentaseGajiEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of Agama Succes");
			
			return result;
	 }
	 
	 
	 
	 

}
