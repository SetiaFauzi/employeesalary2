package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary2.DTO.KaryawanDTO;
import com.example.EmployeeSalary2.DTO.TunjanganPegawaiDTO;
import com.example.EmployeeSalary2.Repository.TunjanganPegawaiRepository;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary2.models.Karyawan;
import com.example.EmployeeSalary2.models.TunjanganPegawai;

@RestController
@RequestMapping("/api")
public class TunjanganPegawaiController {
	
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	TunjanganPegawaiRepository tunjPegawaiRepos;
	
	
	 @GetMapping("/tunjanganpegawai/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			ArrayList<TunjanganPegawai> listTunjanganPegawaiEntity = (ArrayList<TunjanganPegawai>) tunjPegawaiRepos.findAll();
			ArrayList<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
		
			for(TunjanganPegawai tunjanganPegawai : listTunjanganPegawaiEntity) {
				TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
				listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
			}
			
			result.put("Status", 200);
			result.put("Message", "Read all Karyawan data succes");
			result.put("Data", listTunjanganPegawaiDTO);
			
			return result;
			
	 }
	 
	 @PostMapping("/tunjanganpegawai/create")
		public HashMap<String, Object> createTunjanganPegawaiDTOMaping(@Valid @RequestBody TunjanganPegawaiDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
		
			TunjanganPegawai tunjanganPegawaiEntity = modelMapper.map(body, TunjanganPegawai.class);
			tunjPegawaiRepos.save(tunjanganPegawaiEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New tunjanganpegawai Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	 
	 @PutMapping("/tunjanganpegawai/update/{id}")
		public HashMap<String, Object> updateTunjanganPegawaiDTOMaping(@PathVariable(value = "id") Integer idTunjanganPegawai,
	            @Valid @RequestBody TunjanganPegawaiDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			TunjanganPegawai tunjanganPegawaiEntity = tunjPegawaiRepos.findById(idTunjanganPegawai)
					.orElseThrow(() -> new ResourceNotFoundException ("tunjanganpegawai", "id", idTunjanganPegawai));
			
			tunjanganPegawaiEntity = modelMapper.map(body, TunjanganPegawai.class);
			tunjanganPegawaiEntity.setIdTunjanganPegawai(idTunjanganPegawai);
			
			tunjPegawaiRepos.save(tunjanganPegawaiEntity);
			body = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New tunjanganpegawai Succes");
			result.put("Data", body);
			
			return result;
	 }
	 
	 @DeleteMapping("/tunjanganpegawai/delete/{id}")
		public HashMap<String, Object> deleteTunjanganPegawaiDTOMaping(@PathVariable(value = "id") Integer idTunjanganPegawai,
				TunjanganPegawaiDTO tunjanganPegawaiDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			TunjanganPegawai tunjanganPegawaiEntity = tunjPegawaiRepos.findById(idTunjanganPegawai)
					.orElseThrow(() -> new ResourceNotFoundException ("tunjanganpegawai", "id", idTunjanganPegawai));
			
			tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
			tunjanganPegawaiEntity.setIdTunjanganPegawai(idTunjanganPegawai);
			
			tunjPegawaiRepos.delete(tunjanganPegawaiEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of tunjanganpegawai Succes");
			
			return result;
	
	 }
	
	
	
	


}
