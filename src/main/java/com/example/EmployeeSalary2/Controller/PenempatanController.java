package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.EmployeeSalary2.DTO.PenempatanDTO;
import com.example.EmployeeSalary2.Repository.PenempatanRepository;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary2.models.Penempatan;

@RestController
@RequestMapping("/api")
public class PenempatanController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PenempatanRepository penempatanRepository;
	
	
	
	 @GetMapping("/penempatan/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			ArrayList<Penempatan> listPenempatanEntity = (ArrayList<Penempatan>) penempatanRepository.findAll();
			ArrayList<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
			
			for(Penempatan penempatan : listPenempatanEntity) {
				PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
				listPenempatanDTO.add(penempatanDTO);
			}
			
			result.put("Status", 200);
			result.put("Message", "Read all Penempatan data succes");
			result.put("Data", listPenempatanDTO);
			
			return result;
			
	 }
	 
	 @PostMapping("/penempatan/create")
		public HashMap<String, Object> createPenempatanDTOMaping(@Valid @RequestBody PenempatanDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Penempatan penempatanEntity = modelMapper.map(body, Penempatan.class);
			penempatanRepository.save(penempatanEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New Penempatan Succes");
			result.put("Data", body);
			
			return result;
			
			
	 }
	 
	 @PutMapping("/penempatan/update/{id}")
		public HashMap<String, Object> updatePenempatanDTOMaping(@PathVariable(value = "id") Integer idPenempatan,
	            @Valid @RequestBody PenempatanDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan)
					.orElseThrow(() -> new ResourceNotFoundException ("Penempatan", "id", idPenempatan));
			
			
			penempatanEntity = modelMapper.map(body, Penempatan.class);
			penempatanEntity.setIdPenempatan(idPenempatan);
			
			penempatanRepository.save(penempatanEntity);
			body = modelMapper.map(penempatanEntity, PenempatanDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New Penempatan Succes");
			result.put("Data", body);
			
			return result;
			
			
	 }
	
	 @DeleteMapping("/penempatan/delete/{id}")
		public HashMap<String, Object> deletePenempatanDTOMaping(@PathVariable(value = "id") Integer idPenempatan,
				PenempatanDTO penempatanDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan)
					.orElseThrow(() -> new ResourceNotFoundException ("Penempatan", "id", idPenempatan));
			
			penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
			penempatanEntity.setIdPenempatan(idPenempatan);
			
			penempatanRepository.delete(penempatanEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of Penempatan is Succesed");
			
			return result;
		
			
			
			
			
	 }
	
	
	
	
	
	
	
	
	
	

}
