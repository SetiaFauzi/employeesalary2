package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary2.DTO.KaryawanDTO;
import com.example.EmployeeSalary2.Repository.KaryawanRepository;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary2.models.Karyawan;



@RestController
@RequestMapping("/api")
public class KaryawanController {

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	KaryawanRepository karyawanRepository;
	
	 @GetMapping("/karyawan/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			ArrayList<Karyawan> listKaryawanEntity = (ArrayList<Karyawan>) karyawanRepository.findAll();
			ArrayList<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
			ModelMapper modelMapper = new ModelMapper();
			for(Karyawan karyawan : listKaryawanEntity) {
				KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
				listKaryawanDTO.add(karyawanDTO);
			}
			
			result.put("Status", 200);
			result.put("Message", "Read all Karyawan data succes");
			result.put("Data", listKaryawanDTO);
			
			return result;
			
	 }
		
	 @PostMapping("/karyawan/create")
		public HashMap<String, Object> createKaryawanDTOMaping(@Valid @RequestBody KaryawanDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Karyawan karyawanEntity = modelMapper.map(body, Karyawan.class);
			karyawanRepository.save(karyawanEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New karyawan Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	 
	 @PutMapping("/karyawan/update/{id}")
		public HashMap<String, Object> updateKaryawanDTOMaping(@PathVariable(value = "id") Integer idKaryawan,
	            @Valid @RequestBody KaryawanDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan)
					.orElseThrow(() -> new ResourceNotFoundException ("Karyawan", "id", idKaryawan));
			
			karyawanEntity = modelMapper.map(body, Karyawan.class);
			karyawanEntity.setIdKaryawan(idKaryawan);
			
			karyawanRepository.save(karyawanEntity);
			body = modelMapper.map(karyawanEntity, KaryawanDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New Karyawan Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	 
	 @DeleteMapping("/karyawan/delete/{id}")
		public HashMap<String, Object> deleteKaryawanDTOMaping(@PathVariable(value = "id") Integer idKaryawan,
				KaryawanDTO karyawanDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan)
					.orElseThrow(() -> new ResourceNotFoundException ("Karyawan", "id", idKaryawan));
			
			karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
			karyawanEntity.setIdKaryawan(idKaryawan);
			
			karyawanRepository.delete(karyawanEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of Karyawan Succes");
			
			return result;
			
			
	 }
	
	
	
	
	
	
	
}
