package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary2.DTO.PosisiDTO;
import com.example.EmployeeSalary2.Repository.PosisiRepository;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary2.models.Posisi;

@RestController
@RequestMapping("/api")
public class PosisiController {
	
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PosisiRepository posisiRepository;
	
	
	 @GetMapping("/posisi/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			ArrayList<Posisi> listPosisiEntity = (ArrayList<Posisi>) posisiRepository.findAll();
			ArrayList<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
			ModelMapper modelMapper = new ModelMapper();
			for(Posisi posisi : listPosisiEntity) {
				PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
				listPosisiDTO.add(posisiDTO);
			}

			result.put("Status", 200);
			result.put("Message", "Read all Posisi data succes");
			result.put("Data", listPosisiDTO);
			
			return result;
			
	 }
	 
	 @PostMapping("/posisi/create")
		public HashMap<String, Object> createPosisiDTOMaping(@Valid @RequestBody PosisiDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Posisi posisiEntity = modelMapper.map(body, Posisi.class);
			posisiRepository.save(posisiEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New Posisi Succes");
			result.put("Data", body);
			
			return result;	
	 
	 }
	 
	 @PutMapping("/posisi/update/{id}")
		public HashMap<String, Object> updatePosisiDTOMaping(@PathVariable(value = "id") Integer idPosisi,
	            @Valid @RequestBody PosisiDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Posisi posisiEntity = posisiRepository.findById(idPosisi)
					.orElseThrow(() -> new ResourceNotFoundException ("Posisi", "id", idPosisi));
			
			posisiEntity = modelMapper.map(body, Posisi.class);
			posisiEntity.setIdPosisi(idPosisi);
			
			posisiRepository.save(posisiEntity);
			body = modelMapper.map(posisiEntity, PosisiDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New Posisi Succes");
			result.put("Data", body);
			
			return result;
			
	 }
	 
	 @DeleteMapping("/posisi/delete/{id}")
		public HashMap<String, Object> deletePosisiDTOMaping(@PathVariable(value = "id") Integer idPosisi,
				PosisiDTO posisiDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();

			Posisi posisiEntity = posisiRepository.findById(idPosisi)
					.orElseThrow(() -> new ResourceNotFoundException ("Posisi", "id", idPosisi));
			
			posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
			posisiEntity.setIdPosisi(idPosisi);
			
			posisiRepository.delete(posisiEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of Posisi is Succesed");
			
			return result;	
			
	 }
			
			
	

}
