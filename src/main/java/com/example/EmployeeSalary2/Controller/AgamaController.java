package com.example.EmployeeSalary2.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary2.DTO.AgamaDTO;
import com.example.EmployeeSalary2.Repository.AgamaRepository;
import com.example.EmployeeSalary2.models.Agama;
import com.example.EmployeeSalary2.exceptions.ResourceNotFoundException;



@RestController
@RequestMapping("/api")
public class AgamaController {
	
	 ModelMapper modelMapper = new ModelMapper();
	 
	 @Autowired
	 AgamaRepository agamaRepository;
	 
	 
	 @GetMapping("/agama/read")
		public HashMap<String, Object>getAllDataMaping(){
			HashMap<String, Object> result = new HashMap<String, Object>();
			ModelMapper modelMapper = new ModelMapper();
			
			ArrayList<Agama> listAgamaEntity = (ArrayList<Agama>) agamaRepository.findAll();
			ArrayList<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
		
			for(Agama agama : listAgamaEntity) {
				AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
				listAgamaDTO.add(agamaDTO);
			}
			
			result.put("Status", 200);
			result.put("Message", "Read all Agama data succes");
			result.put("Data", listAgamaDTO);
			
			return result;

	 }
	 
	 @PostMapping("/agama/create")
		public HashMap<String, Object> createAgamaDTOMaping(@Valid @RequestBody AgamaDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
		
			Agama agamaEntity = modelMapper.map(body, Agama.class);
			agamaRepository.save(agamaEntity);
			
			result.put("Status", 200);
			result.put("Message", "Create New Agama Succes");
			result.put("Data", body);
			
			return result;
	
	 }
	 
	 @PutMapping("/agama/update/{id}")
		public HashMap<String, Object> updateAgamaDTOMaping(@PathVariable(value = "id") Integer IdAgama,
	            @Valid @RequestBody AgamaDTO body){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Agama agamaEntity = agamaRepository.findById(IdAgama)
					.orElseThrow(() -> new ResourceNotFoundException ("Agama", "id", IdAgama));
			
			agamaEntity = modelMapper.map(body, Agama.class);
			agamaEntity.setIdAgama(IdAgama);
			
			agamaRepository.save(agamaEntity);
			body = modelMapper.map(agamaEntity, AgamaDTO.class);
			
			result.put("Status", 200);
			result.put("Message", "Update New Agama Succes");
			result.put("Data", body);
			
			return result;
	 }
	 
	 @DeleteMapping("/agama/delete/{id}")
		public HashMap<String, Object> deleteAgamaDTOMaping(@PathVariable(value = "id") Integer IdAgama,
				AgamaDTO agamaDTO){
			HashMap<String, Object> result = new HashMap<String, Object>();
			
			Agama agamaEntity = agamaRepository.findById(IdAgama)
					.orElseThrow(() -> new ResourceNotFoundException ("Agama", "id", IdAgama));
			
			agamaEntity = modelMapper.map(agamaDTO, Agama.class);
			agamaEntity.setIdAgama(IdAgama);
			
			agamaRepository.delete(agamaEntity);
			
			result.put("Status", 200);
			result.put("Message", "Delete One of Agama Succes");
			
			return result;
			
		
			
	 }
	 
 
	

}
